include Makefile.common

REDHAT:=$(shell pwd)
RPMBUILD:=$(REDHAT)/rpmbuild
SCRIPTS:=$(REDHAT)/scripts
KOJI_OPTIONS:=$(KOJI_FLAGS) --scratch

ifeq ($(USE_BINARY), 0)
  BINARY_FLAG=--no-binary
else
  BINARY_FLAG=
endif

# Options section

# Hide command calls without debug option
ifeq ($(DEBUG),1)
  DS=
else
  DS=@
endif

# Hide progress bar in scripts
ifeq ($(NOPROGRESS),1)
  KOJI_OPTIONS:=$(KOJI_OPTIONS) --noprogress
endif

# Do not wait for build finish
ifeq ($(NOWAIT),1)
  KOJI_OPTIONS:=$(KOJI_OPTIONS) --nowait
endif


# create an empty localversion file if you don't want a local buildid
ifneq ($(NOLOCALVERSION),1)
  ifeq ($(LOCALVERSION),)
    LOCALVERSION:=$(shell cat localversion 2>/dev/null)
  endif
  ifeq ($(LOCALVERSION),)
    LOCALVERSION:=$(shell id -u -n)$(shell date +"%Y%m%d%H%M")
  endif
else
  LOCALVERSION:=
endif


.PHONY: rh-clean-sources rh-prep rh-srpm rh-rhel-koji rh-centos-koji rh-help
all: rh-help

rh-clean-sources:
	$(DS)for dname in BUILD RPMS SOURCES SPECS SRPMS; do \
		mkdir -p $(RPMBUILD)/$${dname}; \
	done;
	$(DS)for i in $(RPMBUILD)/SOURCES/*; do \
		rm -f $$i; \
	done;

rh-format-patch: rh-clean-sources
	$(DS)cd `git rev-parse --show-toplevel` && \
	git format-patch $(BINARY_FLAG) --no-numbered --no-signature -o "$(RPMBUILD)/SOURCES" "$(MARKER)" \
	-- ':(exclude).git*' ':(exclude)*.spec' ':(exclude).distro' ':(exclude)redhat'

rh-prep: rh-format-patch
	$(DS)if [ ! -f $(TARFILE) ]; then \
		wget $(TARURL); \
		if [ -n "$(TARSHA512)" -a -e $(REDHAT)/$(TARFILE) -a "`$(SCRIPTS)/tarball_checksum.sh $(TARFILE)`" != "$(TARSHA512)" ]; then \
			echo "$(TARFILE) sha512sum does not match (expected: $(TARSHA512) / got `$(SCRIPTS)/tarball_checksum.sh $(TARFILE)`)"; \
			exit 1; \
		fi; \
	fi;
	$(DS)if [ -n "$(SOURCES_FILELIST)" ]; then \
		echo "Copying Sources: $(SOURCES_FILELIST)"; \
		cp $(SOURCES_FILELIST) $(RPMBUILD)/SOURCES; \
	fi
	$(DS)$(SCRIPTS)/process_patches.sh "$(TARFILE)" "$(SPECFILE)" "$(MARKER)" "$(LOCALVERSION)"

rh-srpm: rh-prep
	$(DS)rpmbuild --define "_sourcedir $(RPMBUILD)/SOURCES" --define "_builddir $(RPMBUILD)/BUILD" --define "_srcrpmdir $(RPMBUILD)/SRPMS" --define "_rpmdir $(RPMBUILD)/RPMS" --define "_specdir $(RPMBUILD)/SPECS" --define "dist $(DIST)" --nodeps -bs $(RPMBUILD)/SPECS/$(SPECFILE)

rh-rhel-koji: rh-srpm
	@echo "Build $(SRPM_NAME) as $(BUILD_TARGET_RHEL)"
	$(DS)brew build $(KOJI_OPTIONS) $(BUILD_TARGET_RHEL) $(RPMBUILD)/SRPMS/$(SRPM_NAME)

rh-centos-koji: rh-srpm
	@echo "Build $(SRPM_NAME) as $(BUILD_TARGET_CENTOS)"
	$(DS)koji --profile=stream build $(KOJI_OPTIONS) $(BUILD_TARGET_CENTOS) $(RPMBUILD)/SRPMS/$(SRPM_NAME)

rh-help:
	@echo "Supported make targets:"
	@echo "  rh-srpm:        Create srpm"
	@echo "  rh-rhel-koji:   Build package using RHEL 9 koji"
	@echo "  rh-centos-koji: Build package using CentOS 9 Streams koji"
	@echo "  rh-help:        Print out help"
	@echo ""
	@echo "Supported variables:"
	@echo "  NOPROGRESS:     If 1, do not show progressbar when uploading srpm"
	@echo "  NOWAIT:         If 1, do not wait for build to finish"
	@echo "  NOLOCALVESION:  If 1, do append LOCALVERSION to release version"
	@echo "  LOCALVERSION:   Use value as release version suffix"
	@echo""
	@echo "LOCALVERSION can be stored in .distro/localversion file."
	@echo "If LOCALVERSION is not set and there's no (or empty) .distro/localversion file,"
	@echo "date +"%Y%m%d%H%M" is used instead."
